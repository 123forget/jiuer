const path = require("path");

module.exports = {
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "scss",
      patterns: [path.resolve(__dirname, "./src/assets/scss/*.scss")],
    },
  },
  devServer: {
    disableHostCheck: true
  },
  productionSourceMap:false,

  lintOnSave: false,
};
