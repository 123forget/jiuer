import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);

export default new Router({
  mode: 'history', //去掉url中的#
  routes: [{
      path: '/',
      redirect: {
        name: '首页'
      }
    },
    {
      path: "/shouye/:address",
      name: "首页",
      component: () => import('../view/index/index.vue')
    }, {
      //二手房
      path: '/ershoufang/:address',
      name: '二手房',
      component: () => import("../view/secondHandHouse/secondHandHouse.vue")
    }, {
      // 二手房详情
      path: '/houseDetail/:id',
      name: 'houseDetail',
      component: () => import("../view/houseDetail/houseDetail.vue")
    }, {
      // 新房
      path: '/xingfang/:address',
      name: '新房',
      component: () => import("../view/newHouse/newHouse.vue")
    }, {
      //  新房详情
      path: '/newHouseDetail/:id',
      name: 'newHouseDetail',
      component: () => import("../view/newHouseDetail/newHouseDetail.vue")
    }, {
      //  新房评论
      path: '/newHouseTab/:address/:id',
      name: 'newHouseTab',
      component: () => import("../view/newHouseTab/newHouseTab.vue")
    }, {
      // 租房
      path: '/zhufang/:address',
      name: '租房',
      component: () => import("../view/renting/renting.vue")
    }, {
      // 租房详情
      path: '/rentingDetail/:id',
      name: 'rentingDetail',
      component: () => import("../view/rentingDetail/rentingDetail.vue")
    }, {
      // 小区
      path: '/xiaoqu/:address',
      name: '小区',
      component: () => import("../view/quarters/quarters.vue")
    }, {
      // 小区详情
      path: '/quartersDetail/:id',
      name: 'quartersDetail',
      component: () => import("../view/quartersDetail/quartersDetail.vue")
    }, {
      // 我是业主
      path: '/woshiyezhu/:address',
      name: '我是业主',
      component: () => import("../view/owner/owner.vue")
    }, {
      // 个人中心
      path: '/personalCenter/:address',
      name: '个人中心',
      component: () => import("../view/personalCenter/personalCenter.vue")
    }, {
      // 研究资料
      path: '/yanjiuziliao/:address',
      name: '研究资料',
      component: () => import("../view/research/research.vue")
    }, {
      // 研究资料详情
      path: '/researchDetails/:id',
      name: 'researchDetails',
      component: () => import("../view/researchDetails/researchDetails.vue")
    }, {
      //授权登录页面
      path: "/callback",
      name: 'callback',
      component: () => import("../view/callback/callback.vue")
    }, {
      // error
      path: '*',
      name: 'error',
      component: () => import("../view/error/error.vue")
    }

  ]

});