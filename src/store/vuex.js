import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    /**显示/隐藏登录弹框 */
    loginShow: false,
    /**控制注册弹框样式 */
    registersType: '',
    /**控制显示或隐藏聊天框 */
    isShow: false,
    /**修改头部样式 */
    status: 0,
    id: "",
    /**用户名登录成功状态 */
    successful: false,
    /**控制微信弹框（显示/隐藏） */
    wxPop: false,
    /**存储区域地址关键字 */
    regionKeyword: {}
  },
  mutations: {
    /**存储token */
    tokenFun(state, e) {
      state.successful = e;
    },
    /**控制显示或隐藏聊天框方法 */
    show(state, e) {
      state.isShow = e
    },
    /**控制头部样式方法 */
    sytleFun(state, id) {
      state.id = id;
    },
    state(state, e) {
      state.status = e;
    },
    /**控制登录框显示或隐藏方法 */
    loginPop(state, e) {
      state.loginShow = e
    },
    /**控制注册框显示或隐藏方法 */
    registers(state, e) {
      state.loginShow = e.states;
      state.registersType = e.registersType
    },
    /**更改登录成功状态 */
    loginSuccessful(state, e) {
      state.userMssage = e;
    },
    /**控制微信弹框的方法 */
    wxPopFun(state, e) {
      state.wxPop = e
    },
    /**区域关键字 */
    region(state,e){
      state.regionKeyword=e;
    }
  }
})
export default store;