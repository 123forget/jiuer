import {
  get,
  post
} from "./http";
/***************** 登录模块 ********************/
/** 登录接口 */
export function login(params) {
  return post('/customer/login', params);
}
/**注册 */
export function register(params) {
  return post('/customer/register', params);
}
/**获取验证码 */
export function code(data) {
  return get(`/getVerificationCode/${data.phone}?status=${data.status}`);
}
/**忘记密码 */
export function forgetPassword(params) {
  return post('/customer/forgetPassword', params);
}
/**退出 */
export function loginOut(id) {
  return post(`/customer/signOut/${id}`);
}
/**修改密码 */
export function updatePassword(params) {
  return post('/customer/update', params);
}
/**请求微信扫码页面路径接口 */
export function codeHtml(data) {
  return get(`/render?routUrl=${data.routUrl}`);
}
/**调用获取用户基本信息接口*/
export function thirdLogin(data) {
  return get(`/thirdLogin/${data.source}?code=${data.code}&state=${data.state}`);
}
/** 第三方登录接口 */
export function bindThird(data) {
  return post('/customer/bindThird',data);
}
/**客户获取已绑定第三方接口 */
export function getBindThird(data) {
  return post(`/customer/getBindThird?customerId=${data}`);
}
/***************** 登录模块 ********************/


/***************** 首页模块 ********************/
/**获取首页图片配置*/
export function getIndexImage(data) {
  return post('/image/getIndex',data);
}
/***************** 首页模块 ********************/


/***************** 二手房模块 ********************/
/***根据id查询二手房 */
export function secondHandHouseQueryId(id) {
  return get(`/secondHandHouse/${id}`);
}
/**分页查询二手房 */
export function secondHandHouseQuery(params) {
  return post('/secondHandHouse/page/query', params);
}
/**按搜索标签分页查询二手房 */
export function secondHandHouseSearch(params) {
  return post('/secondHandHouse/query', params);
}
/**创建二手房浏览记录 */
export function addBrowseSecond(params) {
  return post(`/customerRecord/addSecondHand?secondHandId=${params.secondHandId}` + `&customerId=${params.customerId}`);
}
/**展示二手房浏览记录 */
export function exhibitionSecond(params) {
  return post(`/customerRecord/secondHandList?customerId=${params.customerId}` + `&pageNum=${params.pageNum}&pageSize=${params.pageSize}`);
}
/**删除二手房浏览记录 */
export function deletleSecond(params) {
  return post('/customerRecord/deleteSecondHandByIds', params);
}
/***************** 二手房模块 ********************/



/***************** 小区模块 ********************/
/**分页查询小区 */
export function communityQuery(params) {
  return post('/community/page/query', params);
}
/**根据id查询小区 */
export function communityQueryId(id) {
  return get(`/community/${id}`);
}
/**创建小区浏览记录 */
export function addBrowseCommunity(params) {
  return post(`/customerRecord/addCommunity?secondHandId=${params.secondHandId}` + `&customerId=${params.customerId}`);
}
/**展示小区浏览记录 */
export function exhibitionCommunity(params) {
  return post(`/customerRecord/communityList?customerId=${params.customerId}` + `&pageNum=${params.pageNum}&pageSize=${params.pageSize}`);
}
/**删除小区浏览记录 */
export function deletleCommunity(params) {
  return post('/customerRecord/deleteByCommunityIds', params);
}
/**分页查询小区成交记录 */
export function communityOrder(params) {
  return post('/communityOrder/page/query', params);
}

/***************** 小区模块 ********************/



/***************** 租房模块 ********************/
export function rentingQuery(params) {
  return post('/renting/page/query', params);
}
/**根据id查询租房 */
export function rentingQueryId(id) {
  return get(`/renting/${id}`);
}
/**创建租房浏览记录 */
export function addBrowseRenting(params) {
  return post(`/customerRecord/addRent?secondHandId=${params.secondHandId}` + `&customerId=${params.customerId}`);
}
/**展示租房浏览记录 */
export function exhibitionRenting(params) {
  return post(`/customerRecord/rentList?customerId=${params.customerId}` + `&pageNum=${params.pageNum}&pageSize=${params.pageSize}`);
}
/**删除租房浏览记录 */
export function deletleRenting(params) {
  return post('/customerRecord/deleteRentByIds', params);
}
/***************** 租房模块 ********************/



/***************** 新房模块 ********************/
export function newHoseQuery(params) {
  return post('/newHouse/page/query', params);
}
export function newHoseQueryId(id) {
  return get(`/newHouse/${id}`);
}
/**创建新房浏览记录 */
export function addBrowseNewHouse(params) {
  return post(`/customerRecord/addNewHouse?secondHandId=${params.secondHandId}` + `&customerId=${params.customerId}`);
}
/**展示新房浏览记录 */
export function exhibitionNewHouse(params) {
  return post(`/customerRecord/newHouseList?customerId=${params.customerId}` + `&pageNum=${params.pageNum}&pageSize=${params.pageSize}`);
}
/**删除新房浏览记录 */
export function deletleNewHouse(params) {
  return post('/customerRecord/deleteByNewHouseIds', params);
}
/***************** 新房模块 ********************/

/***************** 楼盘动态&评论模块 ********************/
/**分页查询楼盘动态*/
export function newHouseDynamicQuery(params) {
  return post('/newHouseDynamic/page/query', params);
}
/**分页查询楼盘评论*/
export function newHouseCommentQuery(params) {
  return post('/newHouseComment/page/query', params);
}
/** 系统文件查询*/
export function queryFile(params) {
  return post('/api/file/query', params);
}
/***************** 楼盘动态模块 ********************/

/***************** 我是业主模块 ********************/
/**添加租房/卖房 */
export function addRenting(params) {
  return post('/sellRent/add', params);
}
/**房源估价 */
export function addAppraisal(params) {
  return post('/appraisal/add', params);
}
export function querySellRent(params) {
  return post('/sellRent/page/query', params);
}
/***************** 我是业主模块 ********************/


/***************** 字典模块 ********************/
/**分页查询字典 */
export function dictionaryQuery(params) {
  return post('/dictionary/page/query', params);
}
/***************** 字典模块 ********************/



/***************** 区域模块 ********************/
/**分页查询区域*/
export function areaCityQuery(params) {
  return post('/areaCity/page/query', params);
}
/**根据id查询地区 */
export function areaCityId(id) {
  return get(`/areaCity/${id}`);
}
/***************** 区域模块 ********************/

/***************** 房源关注模块 ********************/
/**关注房源 */
export function addHouseFollow(params) {
  return post('/houseFollow/add', params);
}
/**查询关注房源 */
export function queryHouseFollow(params) {
  return get(`/houseFollow/isFollow?houseId=${params.houseId}&customerId=${params.customerId}&houseType=${params.houseType}`);
}
/**批量删除关注房源 */
export function deleteByIdsFollow(params) {
  return post('/houseFollow/deleteByIds', params);
}
/**查询二手房源关注 */
export function querySecondHouseFollow(params) {
  return post('/houseFollow/page/querySecondHouse', params);
}
/**查询租房关注 */
export function queryRentingFollow(params) {
  return post('/houseFollow/page/queryRenting', params);
}
/**查询新房关注 */
export function queryNewHouseFollow(params) {
  return post('/houseFollow/page/queryNewHouse', params);
}
/**查询小区关注 */
export function queryCommunityFollow(params) {
  return post('/houseFollow/page/queryCommunity', params);
}

/***************** 房源关注模块 ********************/

/***************** 研究资料模块 ********************/
/**研究资料 */
export function research(params) {
  return post('/researchMaterial/page/query', params);
}
/**研究资料分类菜单 */
export function researchMenu(params) {
  return post('/researchMaterialCategory/page/query', params);
}
/**研究资料详情 */
export function researchDetail(params) {
  return get(`/researchMaterial/${params}`);
}
/***************** 研究资料模块 ********************/

/***************** 经纪人模块 ********************/
/***经济分页查询 */
export function brokerPage(params) {
  return post('/broker/page/query', params);
}
/**经纪人带看记录 */
export function brokerRecord(params) {
  return post('/brokerRecord/page/query', params);
}
/***************** 经纪人模块 ********************/