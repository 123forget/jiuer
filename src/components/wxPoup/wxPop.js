import wxPoup from "./WxPoup.vue"
const wxPop = {
    install: function (Vue) {
        Vue.component('wxPop', wxPoup)
    }
}
export default wxPoup;