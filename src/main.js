import Vue from 'vue'
import App from './App.vue'
import router from "./router/router";
import store from "./store/vuex"
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from "axios";
Vue.prototype.$axios = axios;
import LoginPop from './components/loding/LoginPop.vue';
import WxPoup from './components/wxPoup/WxPoup.vue';
import md5 from 'js-md5';
import VueLazyload from 'vue-lazyload'
/**图片懒加载配置 */
Vue.use(VueLazyload, {
  preLoad: 1.3,
  attempt: 3,
  error: require('./assets/img/index/sale.png'),
  loading: require('./assets/img/loding/loding.gif')
})
Vue.prototype.$md5 = md5;
Vue.use(LoginPop);
Vue.use(WxPoup);
Vue.config.productionTip = false
Vue.use(ElementUI);

/**拦截微信登录操作 */
router.beforeEach((to, from, next) => {
  if (window.location.href.indexOf('code') >= 0) {
    //如果url中包含code,则保存到store中
    let code = window.location.href.split("?")[1];
    let state = code.match(/state=(\S*)/)[1];
    code = code.substring(5, code.indexOf('&'));
    let getCode = sessionStorage.getItem('code');
    let getState = sessionStorage.getItem('state');
    if (getCode == code && getState == state) {
      sessionStorage.removeItem('code');
      sessionStorage.removeItem('state');
    } else {
      /**将微信code存储到本地 */
      sessionStorage.setItem('state', state);
      sessionStorage.setItem("code", code);
    }
    if (code) {
      next();
    } else {
      next({
        path: "/callback"
      });
    }
  } else {
    next();
  }
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')